import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import render


@login_required
def home(request):
    today = datetime.datetime.now().date()
    return render(request, "home.html", {"today": today})
