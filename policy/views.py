from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from formtools.wizard.views import SessionWizardView

from policy.forms import PolicyForm
from policy.forms import PolicyFlowForm
from policy.models import Policy


FORMS = [("addpolicy1", PolicyForm),
         ("addpolicy2", PolicyFlowForm)]

TEMPLATES = {"addpolicy1": "policy/policy_add1.html",
             "addpolicy2": "policy/policy_add2.html"}


@login_required
def policy_home(request):
    policy_list = Policy.objects.all()
    return render(request, "policy/policy.html", {'policy_list': policy_list})


@login_required
def policy_add(request):
    form = PolicyForm()
    return render(request, "policy/policy_add1.html", {"form": form})


@login_required
def policy_edit(request, policy_name):
    policyToEdit = Policy.objects.get(name=policy_name)
    return render(request, "policy/policy_edit.html", {"policyToEdit": policyToEdit})


class PolicyWizard(SessionWizardView):
    def get_template_names(self):
        return [TEMPLATES[self.steps.current]]

    def done(self, form_list, **kwargs):
        return HttpResponseRedirect('/policy/')
