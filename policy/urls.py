from django.conf.urls import url
from views import FORMS, PolicyWizard
from . import views

urlpatterns = [
    url(r'^$', views.policy_home, name='policy'),
    url(r'^add/$', PolicyWizard.as_view(FORMS), name='policy_add'),
    url(r'^edit/([a-zA-Z0-9_]*)/$', views.policy_edit, name='policy_edit'),
]
