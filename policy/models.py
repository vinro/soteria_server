from django.db import models


class Policy(models.Model):
    DEVICE_TYPES = (
        ('ASR9k', 'ASR9k'),
        ('Nexus', 'Nexus'),
        ('ASA', 'ASA'),
    )
    name = models.CharField(max_length=30, primary_key=True, unique=True)
    device = models.CharField(max_length=15, choices=DEVICE_TYPES)
    description = models.CharField(max_length=100)
    policy = models.TextField()
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "policy"