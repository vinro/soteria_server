from django import forms
from .models import Policy


class PolicyForm(forms.ModelForm):
    name = forms.CharField(label='Name',
                           widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter a name for '
                                                                                                 'the policy'}),
                           required=True)
    device = forms.ChoiceField(label='Device',
                               widget=forms.Select(attrs={'class': 'form-control'}),
                               choices=Policy.DEVICE_TYPES,
                               required=True)
    description = forms.CharField(label='Description',
                                  widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter a '
                                                                                                        'description '
                                                                                                        'for the '
                                                                                                        'policy'}),
                                  required=False)

    class Meta:
        model = Policy
        fields = ('name', 'device', 'description')


class PolicyFlowForm(forms.ModelForm):
    policy = forms.Textarea()

    class Meta:
        model = Policy
        fields = ('policy',)
