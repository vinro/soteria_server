var numberOfElements = 0;
var htmlBase = 'drawingArea';
jsPlumb.ready(function () {

	//FIX DOM:
	$(("#taskcontainer0"))[0].innerHTML = $(("#taskcontainer0"))[0].innerHTML;

	jsPlumb.draggable($(".window"));

	jsPlumb.setContainer("drawingArea");

	$("#drawingArea").css({
		  "-webkit-transform":"scale(0.25)",
		  "-moz-transform":"scale(0.25)",
		  "-ms-transform":"scale(0.25)",
		  "-o-transform":"scale(0.25)",
		  "transform":"scale(0.25)"
		});

    window.setZoom = function (zoom, instance, transformOrigin, el) {
        transformOrigin = transformOrigin || [0.5, 0.5];
        instance = instance || jsPlumb;
        el = el || instance.getContainer();
        var p = ["webkit", "moz", "ms", "o"],
            s = "scale(" + zoom + ")",
            oString = (transformOrigin[0] * 100) + "% " + (transformOrigin[1] * 100) + "%";

        for (var i = 0; i < p.length; i++) {
            el.style[p[i] + "Transform"] = s;
            el.style[p[i] + "TransformOrigin"] = oString;
        }

        el.style["transform"] = s;
        el.style["transformOrigin"] = oString;

        instance.setZoom(zoom, true);
        instance.repaintEverything();
    };
	var zoom = 0.75;
    setZoom(zoom);


    $('#zoomin').click(function () {
    	if (zoom < 1) {
    		zoom = zoom + 0.1;
		}
		setZoom(zoom);
	});

    $('#zoomout').click(function () {
		if (zoom > 0.2) {
    		zoom = zoom - 0.1;
		}
		setZoom(zoom);
	});

	jsPlumb.importDefaults({
		Endpoint : ["Dot", {radius:8}],
		EndpointStyle : { fillStyle : "#4A6" },
		HoverPaintStyle : {strokeStyle:"#42a62c", lineWidth:8 },
		Connector:[ "Flowchart" ],
		ConnectionOverlays : [
			[ "Arrow", {
				location:1,
				id:"arrow",
				length:20,
				foldback:0.4
			} ]
		]
	});

	var workflowConnectorStartpoint = {
		isSource: true,
		isTarget: false,
		maxConnections: 1,
		anchor:"BottomCenter"
	};

	var workflowConnectorEndpoint = {
		isSource: false,
		isTarget: true,
		maxConnections: -1,
		anchor:"TopCenter",
		paintStyle: { fillStyle: 'red' },
		endpoint: ["Rectangle", {width:12, height:12}]
	};

	jsPlumb.addEndpoint(
		$('.startpoint'),
		workflowConnectorStartpoint
	);

	jsPlumb.addEndpoint(
		$('.endpoint'),
		workflowConnectorEndpoint
	);

	$('#'+htmlBase).on("click", ".button_remove", function () {
		var parentnode = $(this)[0].parentNode.parentNode;
		jsPlumb.removeAllEndpoints(parentnode);
		$(parentnode).remove();
	});

	$(".button_add_task").click(function () {
		addTask();
	});

	$('#saveButton').click(function(){
		saveFlowchart();
	});

	$('#loadButton').click(function(){
		loadFlowchart();
	});

});
function addTask(id){
	if(typeof id === "undefined"){
		numberOfElements++;
		id = "taskcontainer" + numberOfElements;
	}

	$('<div class="window task node" id="' + id + '" data-nodetype="task" style="left:30px; top:30px;">').appendTo('#'+htmlBase).html($(("#taskcontainer0"))[0].innerHTML);

	var taskSourceConnectorEndpointLeft = {
		isSource: true,
		isTarget: false,
		maxConnections: 1,
		anchor:[ "LeftMiddle"],
	};

	var taskSourceConnectorEndpointRight = {
		isSource: true,
		isTarget: false,
		maxConnections: 1,
		anchor:[ "RightMiddle"],
	};

	var taskTargetConnectorEndpoint = {
		isSource: false,
		isTarget: true,
		maxConnections: 1,
		anchor:[ 0.5, 0, 0, -1, 0, 0 , "task_end endpoint"],
		paintStyle: { fillStyle: 'red' },
		endpoint: ["Rectangle", {width:12, height:12}]
	};

	jsPlumb.addEndpoint(
		$('#'+id),
		taskSourceConnectorEndpointLeft
	);

	jsPlumb.addEndpoint(
		$('#'+id),
		taskSourceConnectorEndpointRight
	);

	jsPlumb.addEndpoint(
		$('#'+id),
		taskTargetConnectorEndpoint
	);

	jsPlumb.draggable($('#' + id));
	return id;
}

function saveFlowchart(){
	var nodes = []
	$(".node").each(function (idx, elem) {
	var $elem = $(elem);
	var endpoints = jsPlumb.getEndpoints($elem.attr('id'));
	console.log('endpoints of '+$elem.attr('id'));
	console.log(endpoints);
		nodes.push({
			blockId: $elem.attr('id'),
			nodetype: $elem.attr('data-nodetype'),
			positionX: parseInt($elem.css("left"), 10),
			positionY: parseInt($elem.css("top"), 10)
		});
	});
	var connections = [];
	$.each(jsPlumb.getConnections(), function (idx, connection) {
		connections.push({
			connectionId: connection.id,
			pageSourceId: connection.sourceId,
			pageTargetId: connection.targetId
		});
	});

	var flowChart = {};
	flowChart.nodes = nodes;
	flowChart.connections = connections;
	flowChart.numberOfElements = numberOfElements;

	var flowChartJson = JSON.stringify(flowChart);
	//console.log(flowChartJson);

	$('#jsonOutput').val(flowChartJson);
}
function loadFlowchart(){
	var flowChartJson = $('#jsonOutput').val();
	var flowChart = JSON.parse(flowChartJson);
	var nodes = flowChart.nodes;
	$.each(nodes, function( index, elem ) {
		if(elem.nodetype === 'startpoint'){
			repositionElement('startpoint', elem.positionX, elem.positionY);
		}else if(elem.nodetype === 'endpoint'){
			repositionElement('endpoint', elem.positionX, elem.positionY);
		}else if(elem.nodetype === 'task'){
			var id = addTask(elem.blockId);
			repositionElement(id, elem.positionX, elem.positionY);
		}else if(elem.nodetype === 'decision'){
			var id = addDecision(elem.blockId);
			repositionElement(id, elem.positionX, elem.positionY);
		}else{

		}
	});

	var connections = flowChart.connections;
	$.each(connections, function( index, elem ) {
		 var connection1 = jsPlumb.connect({
			source: elem.pageSourceId,
			target: elem.pageTargetId,
			anchors: ["BottomCenter", [0.75, 0, 0, -1]]

		});
	});

	numberOfElements = flowChart.numberOfElements;
}
function repositionElement(id, posX, posY){
	$('#'+id).css('left', posX);
	$('#'+id).css('top', posY);
	jsPlumb.repaint(id);
}


