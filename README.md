# README #

Welcome to Soteria Server's code repository.

### Some screenshots of the web app ###

![login](./static/images/login.png =250x450)

![policylist](./static/images/policylist.png =250x450)

![addpolicy](./static/images/addpolicy.png =250x450)

![policyflow](./static/images/policyflow.png =250x450)

![logout](./static/images/logout.png =250x450)

### What is this repository for? ###

* This repository contains the code for the server side component of Soteria. The server side component is responsible
for managing all the security policies and analysis jobs. It provides an easy flowchart UI for creating new rules.  
* Version - 0.1

### How do I get set up? ###

* **Summary of set up** 
* **Configuration** Edit the _settings.py_ file inside the server directory for modifying any settings.
* **Dependencies** Django, requests, postgres
* **Deployment instructions** For development purposes, clone this repo onto your laptop and start the Django server.

### Who do I talk to? ###

* Repo owner - Vinay Bilchod Shettru (email: _vinaybsv@gmail.com_, [Linkedin](https://www.linkedin.com/in/vinaybs))
